.\" Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd July 28, 2023
.Dt SARS 5
.Os
.Sh NAME
.Nm sars
.Nd Star AeRoSol file format
.Sh DESCRIPTION
.Nm
is a binary file format for storing the radiative properties of an
aerosol.
The volumetric mesh to which the CKs are attached is
.Em not
described there but must be defined in a separated file via, for example, the
.Xr smsh 5
file format.
.Pp
A
.Nm
file begins by a header that describes the layout of the data.
Next the spectral bands on which the radiative coefficients were defined are
listed.
Finally, the entire list of the scattering coefficients and absorption
coefficients are stored.
.Pp
The header consists of 3 64-bit integers.
The first integer is a power of two
.Pq usually 4096
that defines the
.Va pagesize
in bytes on which the radiative coefficients are aligned.
The remaining two integers store the number of bands and the number of nodes,
that is, the number of radiative coefficients per band which is actually the
number of nodes in the volumetric mesh to which these coefficients are
attached.
.Pp
After the header comes the list of spectral bands sorted in ascending order
relative to their interval.
Each spectral band is defined by 2 double-precision floating-point numbers that
represent the lower and upper limits of the band in nanometers.
.Pp
Fill bytes follow the list of spectral bands to ensure alignment of the
radiative coefficients on
.Va pagesize .
By aligning data on
.Va pagesize ,
and depending on system requirements, memory mapping can be used to
automatically load/unload the radiative coefficients on demand
.Pq see Xr mmap 2 .
For each band, the absorption coefficient and diffusion coefficient are listed
per node.
This list is followed by a
.Va padding ,
which is a list of bytes that provides memory alignment of the following data
to
.Va pagesize .
Bands are sorted according to the order in which they were previously declared.
.Pp
Data are encoded with respect to the little endian bytes ordering, i.e. least
significant bytes are stored first.
.Pp
The file format is as follows:
.Bl -column (per-band-k) (::=) ()
.It Ao Va sars Ac Ta ::= Ta Ao Va pagesize Ac Ao Va #bands Ac Ao Va #nodes Ac
.It Ta Ta Aq Va bands
.It Ta Ta Aq Va padding
.It Ta Ta Aq Va rad-coefs
.It \  Ta Ta
.It Ao Va pagesize Ac Ta ::= Ta Vt uint64_t
.It Ao Va #bands Ac Ta ::= Ta Vt uint64_t
.It Ao Va #nodes Ac Ta ::= Ta Vt uint64_t
.It Ao Va padding Ac Ta ::= Ta Op Va int8_t ...
# Ensure alignment on
.Va pagesize
.It \  Ta Ta
.It Ao Va bands Ac Ta ::= Ta Ao Va band Ac Va ...
.It Ao Va band Ac Ta ::= Ta Ao Va band-low Ac Ao Va band-upp Ac
.It Ao Va band-low Ac Ta ::= Ta Vt double
# Inclusive bound in nm
.It Ao Va band-upp Ac Ta ::= Ta Vt double
# Excusive bound in nm
.It \  Ta Ta
.It Ao Va rad-coefs Ac Ta ::= Ta Ao Va per-band-k Ac Va ...
.It Ao Va ka-ks Ac Ta ::= Ta Ao Va ka Ac Ao Va ks Ac
.It Ao Va ka Ac Ta ::= Ta Vt float
# In m^-1
.It Ao Va ks Ac Ta ::= Ta Vt float
# In m^-1
.El
.Sh SEE ALSO
.Xr mmap 2 ,
.Xr smsh 5
