/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redismshbute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is dismshbuted in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SARS_H
#define SARS_H

#include <rsys/hash.h>
#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SARS_SHARED_BUILD) /* Build shared library */
  #define SARS_API extern EXPORT_SYM
#elif defined(SARS_STATIC) /* Use/build static library */
  #define SARS_API extern LOCAL_SYM
#else /* Use shared library */
  #define SARS_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the smsh function `Func'
 * returns an error. One should use this macro on smsh function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define SARS(Func) ASSERT(sars_ ## Func == RES_OK)
#else
  #define SARS(Func) sars_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

struct sars_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define SARS_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct sars_create_args SARS_CREATE_ARGS_DEFAULT =
  SARS_CREATE_ARGS_DEFAULT__;

struct sars_load_args {
  const char* path;
  int memory_mapping; /* Use memory mapping instead of normal loading */
};
#define SARS_LOAD_ARGS_NULL__ {NULL, 0}
static const struct sars_load_args SARS_LOAD_ARGS_NULL = SARS_LOAD_ARGS_NULL__;

struct sars_load_stream_args {
  FILE* stream;
  const char* name; /* Stream name */
  /* Use memory mapping instead of normal loading. Note that memory mapping
   * cannot be used on some stream like stdin */
  int memory_mapping;
};
#define SARS_LOAD_STREAM_ARGS_NULL__ {NULL, "stream", 0}
static const struct sars_load_stream_args SARS_LOAD_STREAM_ARGS_NULL =
  SARS_LOAD_STREAM_ARGS_NULL__;

struct sars_band {
  double lower; /* Lower band wavelength in nm (inclusive) */
  double upper; /* Upper band wavelength in nm (exclusive) */
  size_t id;

  float* k_list; /* Per node radiative coefficients */
};
#define SARS_BAND_NULL__ {0, 0, 0, NULL}
static const struct sars_band SARS_BAND_NULL = SARS_BAND_NULL__;

/* Forward declaration of opaque data types */
struct sars;

BEGIN_DECLS

/*******************************************************************************
 * Star-AeRoSol API
 ******************************************************************************/
SARS_API res_T
sars_create
  (const struct sars_create_args* args,
   struct sars** sars);

SARS_API res_T
sars_ref_get
  (struct sars* sars);

SARS_API res_T
sars_ref_put
  (struct sars* sars);

SARS_API res_T
sars_load
  (struct sars* sars,
   const struct sars_load_args* args);

SARS_API res_T
sars_load_stream
  (struct sars* sars,
   const struct sars_load_stream_args* args);

/* Validates radiative coefficients. Data checks have already been carried out
 * during loading, notably on spectral bands, but this function performs longer
 * and more thorough tests. It reviews all scattering and absorption
 * coefficients to check their validity, i.e. whether they are positive or zero.
 * Note that checking radiative coefficients is not mandatory, in order to speed
 * up the loading step and avoid loading/unloading them when using memory
 * mapping. */
SARS_API res_T
sars_validate
  (const struct sars* sars);

SARS_API size_t
sars_get_bands_count
  (const struct sars* sars);

SARS_API size_t
sars_get_nodes_count
  (const struct sars* sars);

SARS_API res_T
sars_get_band
  (const struct sars* sars,
   const size_t iband,
   struct sars_band* band);

/* Returns the range of band indices covered by a given spectral range. The
 * returned index range is degenerated (i.e. ibands[0] > ibands[1]) if no band
 * is found */
SARS_API res_T
sars_find_bands
  (const struct sars* sars,
   const double range[2], /* In nm. Limits are inclusive */
   size_t ibands[2]); /* Range of overlaped bands. Limits are inclusive */

SARS_API res_T
sars_band_compute_hash
  (const struct sars* sars,
   const size_t iband,
   hash256_T hash);

SARS_API res_T
sars_compute_hash
  (const struct sars* sars,
   hash256_T hash);

SARS_API const char*
sars_get_name
  (const struct sars* sars);

static INLINE float
sars_band_get_ka(const struct sars_band* band, const size_t inode)
{
  ASSERT(band);
  return band->k_list[inode*2 + 0];
}

static INLINE float
sars_band_get_ks(const struct sars_band* band, const size_t inode)
{
  ASSERT(band);
  return band->k_list[inode*2 + 1];
}

END_DECLS

#endif /* SARS_H */
