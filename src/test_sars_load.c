/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redismshbute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is dismshbuted in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextarfter */

#include "sars.h"
#include <rsys/hash.h>
#include <rsys/mem_allocator.h>
#include <math.h>
#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
check_sars_load
  (const struct sars* sars,
   const size_t nbands,
   const size_t nnodes)
{
  struct sars_band band = SARS_BAND_NULL;
  size_t iband;

  CHK(sars);
  CHK(nbands);
  CHK(nnodes);

  CHK(sars_get_bands_count(sars) == nbands);
  CHK(sars_get_nodes_count(sars) == nnodes);

  CHK(sars_get_band(NULL, 0, &band) == RES_BAD_ARG);
  CHK(sars_get_band(sars, nbands, &band) == RES_BAD_ARG);
  CHK(sars_get_band(sars, nbands, NULL) == RES_BAD_ARG);
  CHK(sars_get_band(sars, 0, &band) == RES_OK);

  FOR_EACH(iband, 0, nbands) {
    const double low = (double)(iband+1);
    const double upp = (double)(iband+2);
    size_t inode;

    CHK(sars_get_band(sars, iband, &band) == RES_OK);
    CHK(band.lower == low);
    CHK(band.upper == upp);
    CHK(band.id == iband);

    FOR_EACH(inode, 0, nnodes) {
      const float ks = (float)(iband*2000 + inode);
      const float ka = (float)(iband*1000 + inode);
      CHK(sars_band_get_ks(&band, inode) == ks);
      CHK(sars_band_get_ka(&band, inode) == ka);
    }
  }
}

static void
write_sars
  (FILE* fp,
   const uint64_t pagesize,
   const uint64_t nbands,
   const uint64_t nnodes)
{
  uint64_t iband;
  const char byte = 0;

  /* Write the header */
  CHK(fwrite(&pagesize, sizeof(pagesize), 1, fp) == 1);
  CHK(fwrite(&nbands, sizeof(nbands), 1, fp) == 1);
  CHK(fwrite(&nnodes, sizeof(nnodes), 1, fp) == 1);

  FOR_EACH(iband, 0, nbands) {
    const double low = (double)(iband+1);
    const double upp = (double)(iband+2);

    /* Write band description */
    CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
    CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  }

  /* Write per band ks and ka */
  FOR_EACH(iband, 0, nbands) {
    uint64_t inode;

    /* Padding */
    CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize), SEEK_SET)==0);

    FOR_EACH(inode, 0, nnodes) {
      const float ka = (float)(iband*1000 + inode);
      const float ks = (float)(iband*2000 + inode);
      CHK(fwrite(&ka, sizeof(ka), 1, fp) == 1);
      CHK(fwrite(&ks, sizeof(ks), 1, fp) == 1);
    }
  }

  /* Padding. Write one char to position the EOF indicator */
  CHK(fseek(fp, (long)ALIGN_SIZE((size_t)ftell(fp), pagesize)-1, SEEK_SET) == 0);
  CHK(fwrite(&byte, sizeof(byte), 1, fp) == 1);
}

static void
test_load(struct sars* sars)
{
  hash256_T hash0;
  hash256_T hash1;
  hash256_T band_hash0;
  hash256_T band_hash1;
  struct sars_load_args args = SARS_LOAD_ARGS_NULL;
  struct sars_load_stream_args stream_args = SARS_LOAD_STREAM_ARGS_NULL;

  FILE* fp = NULL;
  const char* filename = "test_file.sars";
  const uint64_t pagesize = 16384;
  const uint64_t nbands = 11;
  const uint64_t nnodes = 1000;

  CHK(fp = fopen(filename, "w+"));
  write_sars(fp, pagesize, nbands, nnodes);
  rewind(fp);

  stream_args.stream =fp;
  stream_args.name = filename;
  CHK(sars_load_stream(NULL, &stream_args) == RES_BAD_ARG);
  CHK(sars_load_stream(sars, NULL) == RES_BAD_ARG);
  stream_args.stream = NULL;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_OK);

  CHK(!strcmp(sars_get_name(sars), filename));

  CHK(sars_compute_hash(NULL, hash0) == RES_BAD_ARG);
  CHK(sars_compute_hash(sars, NULL) == RES_BAD_ARG);
  CHK(sars_compute_hash(sars, hash0) == RES_OK);

  CHK(sars_band_compute_hash(NULL, 0, band_hash0) == RES_BAD_ARG);
  CHK(sars_band_compute_hash(sars, nbands, band_hash0) == RES_BAD_ARG);
  CHK(sars_band_compute_hash(sars, 0, NULL) == RES_BAD_ARG);
  CHK(sars_band_compute_hash(sars, 0, band_hash0) == RES_OK);
  CHK(!hash256_eq(hash0, band_hash0));

  CHK(sars_band_compute_hash(sars, 0, band_hash1) == RES_OK);
  CHK(hash256_eq(band_hash0, band_hash1));

  check_sars_load(sars, nbands, nnodes);
  rewind(fp);

  stream_args.name = NULL;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  stream_args.name = SARS_LOAD_STREAM_ARGS_NULL.name;
  stream_args.memory_mapping = 1;
  CHK(sars_load_stream(sars, &stream_args) == RES_OK);
  CHK(!strcmp(sars_get_name(sars), SARS_LOAD_STREAM_ARGS_NULL.name));
  check_sars_load(sars, nbands, nnodes);

  args.path = "nop";
  CHK(sars_load(NULL, &args) == RES_BAD_ARG);
  CHK(sars_load(sars, NULL) == RES_BAD_ARG);
  CHK(sars_load(sars, &args) == RES_IO_ERR);
  args.path = filename;
  CHK(sars_load(sars, &args) == RES_OK);
  check_sars_load(sars, nbands, nnodes);

  CHK(sars_compute_hash(sars, hash1) == RES_OK);
  CHK(hash256_eq(hash0, hash1));

  rewind(fp);
  write_sars(fp, pagesize, nbands+1, nnodes);
  rewind(fp);

  stream_args.stream = fp;
  stream_args.name = filename;
  CHK(sars_load_stream(sars, &stream_args) == RES_OK);
  CHK(sars_compute_hash(sars, hash1) == RES_OK);
  CHK(!hash256_eq(hash0, hash1));

  CHK(sars_band_compute_hash(sars, 0, band_hash1) == RES_OK);
  CHK(hash256_eq(band_hash0, band_hash1));

  CHK(fclose(fp) == 0);
}

static void
test_load_fail(struct sars* sars)
{
  struct sars_load_stream_args stream_args = SARS_LOAD_STREAM_ARGS_NULL;
  FILE* fp = NULL;
  double low;
  double upp;

  /* The pagesize is less than the operating system page size*/
  CHK(fp = tmpfile());
  write_sars(fp, 2048, 1, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* The pagesize is not a power of two */
  CHK(fp = tmpfile());
  write_sars(fp, 4100, 1, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong #bands */
  CHK(fp = tmpfile());
  write_sars(fp, 4096, 0, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong #nodes */
  CHK(fp = tmpfile());
  write_sars(fp, 4096, 1, 0);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Wrong band boundaries */
  low = 1;
  upp = 0;
  CHK(fp = tmpfile());
  write_sars(fp, 4096, 1, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Unsorted bands */
  CHK(fp = tmpfile());
  write_sars(fp, 4096, 2, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  low = 1; upp = 2;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  low = 0; upp = 1;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Bands overlap */
  CHK(fp = tmpfile());
  write_sars(fp, 4096, 2, 1);
  CHK(fseek(fp, 24, SEEK_SET) == 0);
  low = 0; upp = 1;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  low = 0.5; upp = 1.5;
  CHK(fwrite(&low, sizeof(low), 1, fp) == 1);
  CHK(fwrite(&upp, sizeof(upp), 1, fp) == 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);
}

static void
test_load_files(struct sars* sars, int argc, char** argv)
{
  int i;
  CHK(sars);
  FOR_EACH(i, 1, argc) {
    hash256_T hash;
    size_t nnodes;
    size_t nbands;
    size_t iband;

    if(!strcmp(argv[i], "-")) { /* Read from stdin */
      struct sars_load_stream_args args = SARS_LOAD_STREAM_ARGS_NULL;
      printf("Load from stdin\n");
      args.stream = stdin;
      args.name = "stdin";
      args.memory_mapping = 1;
      CHK(sars_load_stream(sars, &args) == RES_BAD_ARG);
      args.memory_mapping = 0;
      CHK(sars_load_stream(sars, &args) == RES_OK);
    } else {
      struct sars_load_args args = SARS_LOAD_ARGS_NULL;
      printf("Load %s\n", argv[1]);
      args.path = argv[i];
      args.memory_mapping = 1;
      CHK(sars_load(sars, &args) == RES_OK);
    }
    nbands = sars_get_bands_count(sars);
    nnodes = sars_get_nodes_count(sars);
    CHK(nbands);
    CHK(nnodes);

    FOR_EACH(iband, 0, nbands) {
      struct sars_band band = SARS_BAND_NULL;

      CHK(sars_get_band(sars, iband, &band) == RES_OK);
      printf("band %lu in [%g, %g[ nm\n",
        (unsigned long)band.id,
        band.lower, band.upper);
    }

    CHK(sars_validate(sars) == RES_OK);
    CHK(sars_compute_hash(sars, hash) == RES_OK);
  }
}

static void
test_find(struct sars* sars)
{
  struct sars_load_stream_args stream_args = SARS_LOAD_STREAM_ARGS_NULL;
  size_t ibands[2];
  double range[2];
  FILE* fp;

  CHK(fp = tmpfile());
  write_sars(fp, 4096, 10, 1);
  rewind(fp);
  stream_args.stream = fp;
  CHK(sars_load_stream(sars, &stream_args) == RES_OK);

  range[0] = 0;
  range[1] = 10;
  CHK(sars_find_bands(NULL, range, ibands) == RES_BAD_ARG);
  CHK(sars_find_bands(sars, NULL, ibands) == RES_BAD_ARG);
  CHK(sars_find_bands(sars, range, NULL) == RES_BAD_ARG);
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 9);

  range[0] = 10;
  range[1] = 0;
  CHK(sars_find_bands(sars, range, ibands) == RES_BAD_ARG);

  range[0] = 11;
  range[1] = 12;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 11;
  range[1] = 11;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 0;
  range[1] = nextafter(1, 0);
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 0;
  range[1] = 0;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = nextafter(1, 0);
  range[1] = nextafter(1, 0);
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] > ibands[1]);

  range[0] = 2;
  range[1] = 2;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 1 && ibands[1] == 1);

  range[0] = 0;
  range[1] = 1;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 0);

  range[0] = 1;
  range[1] = nextafterf(2, 1);
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 0);

  range[0] = 2;
  range[1] = 20;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 1 && ibands[1] == 9);

  range[0] = 1.5;
  range[1] = 2;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 0 && ibands[1] == 1);

  range[0] = 3.1;
  range[1] = nextafter(6, 0);
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 2 && ibands[1] == 4);

  range[0] = 3.1;
  range[1] = 7;
  CHK(sars_find_bands(sars, range, ibands) == RES_OK);
  CHK(ibands[0] == 2 && ibands[1] == 6);

  CHK(fclose(fp) == 0);
}

/*******************************************************************************
 * Main function
 ******************************************************************************/
int
main(int argc, char** argv)
{
  struct sars_create_args args = SARS_CREATE_ARGS_DEFAULT;
  struct sars* sars = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(sars_create(&args, &sars) == RES_OK);

  if(argc > 1) {
    test_load_files(sars, argc, argv);
  } else {
    test_load(sars);
    test_load_fail(sars);
    test_find(sars);
  }

  CHK(sars_ref_put(sars) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
