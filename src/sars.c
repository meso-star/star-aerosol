/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200809L /* mmap support */
#define _DEFAULT_SOURCE 1 /* MAP_POPULATE support */
#define _BSD_SOURCE 1 /* MAP_POPULATE for glibc < 2.19 */

#include "sars.h"
#include "sars_c.h"
#include "sars_log.h"

#include <rsys/algorithm.h>
#include <rsys/cstr.h>
#include <rsys/hash.h>

#include <unistd.h> /* sysconf support */

#include <errno.h>
#include <sys/mman.h> /* mmap */
#include <sys/stat.h> /* fstat */

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
is_stdin(FILE* stream)
{
  struct stat stream_buf;
  struct stat stdin_buf;
  ASSERT(stream);
  CHK(fstat(fileno(stream), &stream_buf) == 0);
  CHK(fstat(STDIN_FILENO, &stdin_buf) == 0);
  return stream_buf.st_dev == stdin_buf.st_dev;
}

static INLINE res_T
check_sars_create_args(const struct sars_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static INLINE res_T
check_sars_load_args(const struct sars_load_args* args)
{
  if(!args || !args->path) return RES_BAD_ARG;
  return RES_OK;
}

static INLINE res_T
check_sars_load_stream_args
  (struct sars* sars,
   const struct sars_load_stream_args* args)
{
  if(!args || !args->stream || !args->name) return RES_BAD_ARG;
  if(args->memory_mapping && is_stdin(args->stream)) {
    log_err(sars,
      "%s: unable to use memory mapping on data loaded from stdin\n",
      args->name);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

static void
reset_sars(struct sars* sars)
{
  ASSERT(sars);
  sars->pagesize = 0;
  sars->nnodes = 0;
  darray_band_purge(&sars->bands);
}

static res_T
read_band
  (struct sars* sars,
   struct band* band,
   FILE* stream)
{
  size_t iband;
  res_T res = RES_OK;
  ASSERT(sars && band);

  band->sars = sars;
  iband = (size_t)(band - darray_band_cdata_get(&sars->bands));

  /* Read band definition */
  #define READ(Var,  Name) {                                                   \
    if(fread((Var), sizeof(*(Var)), 1, stream) != 1) {                         \
      log_err(sars, "%s: band %lu: could not read the %s.\n",                  \
        sars_get_name(sars), (unsigned long)iband, (Name));                    \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&band->low, "band lower bound");
  READ(&band->upp, "band upper bound");
  #undef READ

  /* Check band description */
  if(band->low < 0 || band->low >= band->upp) {
    log_err(sars,
      "%s: band %lu: invalid band range [%g, %g[.\n",
      sars_get_name(sars), (unsigned long)iband, band->low, band->upp);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
map_data
  (struct sars* sars,
   const int fd, /* File descriptor */
   const size_t filesz, /* Overall filesize */
   const char* data_name,
   const off_t offset, /* Offset of the data into file */
   const size_t map_len,
   void** out_map) /* Lenght of the data to map */
{
  void* map = NULL;
  res_T res = RES_OK;
  ASSERT(sars && filesz && data_name && map_len && out_map);
  ASSERT(IS_ALIGNED((size_t)offset, (size_t)sars->pagesize));

  if((size_t)offset + map_len > filesz) {
    log_err(sars, "%s: the %s to map exceed the file size\n",
      sars_get_name(sars), data_name);
    res = RES_IO_ERR;
    goto error;
  }

  map = mmap(NULL, map_len, PROT_READ, MAP_PRIVATE|MAP_POPULATE, fd, offset);
  if(map == MAP_FAILED) {
    log_err(sars, "%s: could not map the %s -- %s\n",
      sars_get_name(sars), data_name, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

exit:
  *out_map = map;
  return res;
error:
  if(map == MAP_FAILED) map = NULL;
  goto exit;
}

static res_T
map_file(struct sars* sars, FILE* stream)
{
  size_t filesz;
  size_t map_len;
  size_t iband;
  size_t nbands;
  off_t offset;
  res_T res = RES_OK;
  ASSERT(sars && stream);

  /* Compute the length in bytes of the k to map for each band/quadrature point */
  map_len = ALIGN_SIZE(sars->nnodes * sizeof(float)*2, sars->pagesize);

  /* Compute the offset toward the 1st list of radiative coefficients */
  offset = ftell(stream);
  offset = (off_t)ALIGN_SIZE((uint64_t)offset, sars->pagesize);

  /* Retrieve the overall filesize */
  fseek(stream, 0, SEEK_END);
  filesz = (size_t)ftell(stream);

  nbands = sars_get_bands_count(sars);
  FOR_EACH(iband, 0, nbands) {
    struct band* band = NULL;

    band = darray_band_data_get(&sars->bands) + iband;
    band->map_len = map_len;

    /* Mapping per band radiative coefficients */
    res = map_data(sars, fileno(stream), filesz, "radiative coefficients",
      offset, band->map_len, (void**)&band->k_list);
    if(res != RES_OK) {
      log_err(sars,
        "%s: data mapping error for band %lu\n",
        sars_get_name(sars), (unsigned long)iband);
      res = RES_IO_ERR;
      goto error;
    }

    offset = (off_t)((size_t)offset + map_len);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
read_padding(FILE* stream, const size_t padding)
{
  char chunk[1024];
  size_t remaining_nbytes = padding;

  while(remaining_nbytes) {
    const size_t nbytes = MMIN(sizeof(chunk), remaining_nbytes);
    if(fread(chunk, 1, nbytes, stream) != nbytes) return RES_IO_ERR;
    remaining_nbytes -= nbytes;
  }
  return RES_OK;
}

/* Return the size in bytes of the data layout and band descriptors */
static INLINE size_t
compute_sizeof_header(struct sars* sars)
{
  size_t sizeof_header = 0;
  ASSERT(sars);

  sizeof_header =
    sizeof(uint64_t) /* pagesize */
  + sizeof(uint64_t) /* #bands */
  + sizeof(uint64_t) /* #nodes */
  + sizeof(double[2]) * sars_get_bands_count(sars); /* Bands */
  return sizeof_header;
}

static res_T
load_data
  (struct sars* sars,
   FILE* stream,
   const char* data_name,
   float** out_data)
{
  float* data = NULL;
  res_T res = RES_OK;
  ASSERT(sars && stream && data_name && out_data);

  data = MEM_ALLOC(sars->allocator, sizeof(float[2]/*ka and ks*/)*sars->nnodes);
  if(!data) {
    res = RES_MEM_ERR;
    log_err(sars, "%s: could not allocate the %s -- %s\n",
      sars_get_name(sars), data_name, res_to_cstr(res));
    goto error;
  }

  if(fread(data, sizeof(float[2]), sars->nnodes, stream) != sars->nnodes) {
    res = RES_IO_ERR;
    log_err(sars, "%s: could not read the %s -- %s\n",
      sars_get_name(sars), data_name, res_to_cstr(res));
    goto error;
  }

exit:
  *out_data = data;
  return res;
error:
  if(data) { MEM_RM(sars->allocator, data); data = NULL; }
  goto exit;
}

static res_T
load_file(struct sars* sars, FILE* stream)
{
  size_t sizeof_header;
  size_t sizeof_k_list;
  size_t padding_bytes;
  size_t iband;
  size_t nbands;
  res_T res = RES_OK;
  ASSERT(sars && stream);

  sizeof_header = compute_sizeof_header(sars);
  sizeof_k_list = sizeof(float[2])*sars->nnodes;

  padding_bytes = ALIGN_SIZE(sizeof_header, sars->pagesize) - sizeof_header;
  if((res = read_padding(stream, padding_bytes)) != RES_OK) goto error;

  /* Calculate the padding between the lists of radiative coefficients. Note
   * that this padding is the same between each list */
  padding_bytes = ALIGN_SIZE(sizeof_k_list, sars->pagesize) - sizeof_k_list;

  nbands = sars_get_bands_count(sars);
  FOR_EACH(iband, 0, nbands) {
    struct band* band = NULL;

    band = darray_band_data_get(&sars->bands) + iband;
    ASSERT(!band->k_list && band->sars == sars);

    /* Loading per band scattering coefficients */
    res = load_data(sars, stream, "radiative coefficients", &band->k_list);
    if(res != RES_OK) {
      log_err(sars,
        "%s: data loading error for band %lu\n",
        sars_get_name(sars), (unsigned long)iband);
      goto error;
    }

    if((res = read_padding(stream, padding_bytes)) != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_stream(struct sars* sars, const struct sars_load_stream_args* args)
{
  size_t iband;
  uint64_t nbands;
  res_T res = RES_OK;
  ASSERT(sars && check_sars_load_stream_args(sars, args) == RES_OK);

  reset_sars(sars);

  res = str_set(&sars->name, args->name);
  if(res != RES_OK) {
    log_err(sars, "%s: unable to duplicate path to loaded data or stream name\n",
      args->name);
    goto error;
  }

  /* Read file header */
  #define READ(Var, Name) {                                                    \
    if(fread((Var), sizeof(*(Var)), 1, args->stream) != 1) {                   \
      log_err(sars, "%s: could not read the %s.\n", sars_get_name(sars), (Name));\
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&sars->pagesize, "page size");
  READ(&nbands, "number of bands");
  READ(&sars->nnodes, "number of nodes");
  #undef READ

  /* Check band description */
  if(!IS_ALIGNED(sars->pagesize, sars->pagesize_os)) {
    log_err(sars,
      "%s: invalid page size %lu. The page size attribute must be aligned on "
      "the page size of the operating system (%lu).\n",
      sars_get_name(sars),
      (unsigned long)sars->pagesize,
      (unsigned long)sars->pagesize_os);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!nbands) {
    log_err(sars, "%s: invalid number of bands %lu.\n",
      sars_get_name(sars), (unsigned long)nbands);
    res = RES_BAD_ARG;
    goto error;
  }
  if(!sars->nnodes) {
    log_err(sars, "%s: invalid number of nodes %lu.\n",
      sars_get_name(sars), (unsigned long)sars->nnodes);
    res = RES_BAD_ARG;
    goto error;
  }

  /* Allocate the bands */
  res = darray_band_resize(&sars->bands, nbands);
  if(res != RES_OK) {
    log_err(sars, "%s: could not allocate the list of bands (#bands=%lu).\n",
      sars_get_name(sars), (unsigned long)nbands);
    goto error;
  }

  /* Read the band description */
  FOR_EACH(iband, 0, nbands) {
    struct band* band = darray_band_data_get(&sars->bands) + iband;
    res = read_band(sars, band, args->stream);
    if(res != RES_OK) goto error;
    if(iband > 0 && band[0].low < band[-1].upp) {
      log_err(sars,
        "%s: bands must be sorted in ascending order and must not "
        "overlap (band %lu in [%g, %g[ nm; band %lu in [%g, %g[ nm).\n",
        sars_get_name(sars),
        (unsigned long)(iband-1), band[-1].low, band[-1].upp,
        (unsigned long)(iband),   band[ 0].low, band[ 0].upp);
      res = RES_BAD_ARG;
      goto error;
    }
  }

  if(args->memory_mapping) {
    res = map_file(sars, args->stream);
    if(res != RES_OK) goto error;
  } else {
    res = load_file(sars, args->stream);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  reset_sars(sars);
  goto exit;
}

static INLINE int
cmp_band(const void* key, const void* item)
{
  const struct band* band = item;
  double wnum;
  ASSERT(key && item);
  wnum = *(double*)key;

  if(wnum < band->low) {
    return -1;
  } else if(wnum >= band->upp) {
    return +1;
  } else {
    return 0;
  }
}

static INLINE void
hash_band
  (struct sha256_ctx* ctx,
   const struct sars_band* band,
   const size_t nnodes)
{
  sha256_ctx_update(ctx, (const char*)&band->lower, sizeof(band->lower));
  sha256_ctx_update(ctx, (const char*)&band->upper, sizeof(band->upper));
  sha256_ctx_update(ctx, (const char*)&band->id, sizeof(band->id));
  sha256_ctx_update(ctx, (const char*)band->k_list, sizeof(*band->k_list)*nnodes);
}

static void
release_sars(ref_T* ref)
{
  struct sars* sars = NULL;
  ASSERT(ref);
  sars = CONTAINER_OF(ref, struct sars, ref);
  if(sars->logger == &sars->logger__) logger_release(&sars->logger__);
  str_release(&sars->name);
  darray_band_release(&sars->bands);
  MEM_RM(sars->allocator, sars);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
sars_create
  (const struct sars_create_args* args,
   struct sars** out_sars)
{
  struct sars* sars = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_sars) { res = RES_BAD_ARG; goto error; }
  res = check_sars_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  sars = MEM_CALLOC(allocator, 1, sizeof(*sars));
  if(!sars) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the Star-Aerosol device.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }

  ref_init(&sars->ref);
  sars->allocator = allocator;
  sars->verbose = args->verbose;
  sars->pagesize_os = (size_t)sysconf(_SC_PAGESIZE);
  str_init(allocator, &sars->name);
  darray_band_init(allocator, &sars->bands);
  if(args->logger) {
    sars->logger = args->logger;
  } else {
    setup_log_default(sars);
  }

exit:
  if(out_sars) *out_sars = sars ;
  return res;
error:
  if(sars) { SARS(ref_put(sars)); sars = NULL; }
  goto exit;
}

res_T
sars_ref_get(struct sars* sars)
{
  if(!sars) return RES_BAD_ARG;
  ref_get(&sars->ref);
  return RES_OK;
}

res_T
sars_ref_put(struct sars* sars)
{
  if(!sars) return RES_BAD_ARG;
  ref_put(&sars->ref, release_sars);
  return RES_OK;
}

res_T
sars_load(struct sars* sars, const struct sars_load_args* args)
{
  struct sars_load_stream_args stream_args = SARS_LOAD_STREAM_ARGS_NULL;
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!sars) { res = RES_BAD_ARG; goto error; }
  res = check_sars_load_args(args);
  if(res != RES_OK) goto error;

  file = fopen(args->path, "r");
  if(!file) {
    log_err(sars, "%s: error opening file `%s'.\n", FUNC_NAME, args->path);
    res = RES_IO_ERR;
    goto error;
  }

  stream_args.stream = file;
  stream_args.name = args->path;
  stream_args.memory_mapping = args->memory_mapping;
  res = load_stream(sars, &stream_args);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
sars_load_stream(struct sars* sars, const struct sars_load_stream_args* args)
{
  res_T res = RES_OK;
  if(!sars) return RES_BAD_ARG;
  res = check_sars_load_stream_args(sars, args);
  if(res != RES_OK) return res;
  return load_stream(sars, args);
}

res_T
sars_validate(const struct sars* sars)
{
  size_t iband;
  size_t nbands;
  if(!sars) return RES_BAD_ARG;

  nbands = sars_get_bands_count(sars);
  FOR_EACH(iband, 0, nbands) {
    struct sars_band band = SARS_BAND_NULL;
    size_t inode;
    size_t nnodes;

    SARS(get_band(sars, iband, &band));

    /* Check band limits */
    if(band.lower != band.lower /* NaN? */
    || band.upper != band.upper) { /* NaN? */
      log_err(sars,
        "%s: invalid limits for band %lu: [%g, %g[\n",
        sars_get_name(sars), (unsigned long)iband, band.lower, band.upper);
      return RES_BAD_ARG;
    }

    /* Check radiative coefficients */
    nnodes = sars_get_nodes_count(sars);
    FOR_EACH(inode, 0, nnodes) {
      const float ka = sars_band_get_ka(&band, inode);
      const float ks = sars_band_get_ks(&band, inode);
      if(ka != ka /* NaN? */ || ka < 0) {
        log_err(sars,
          "%s: invalid absorption coefficient for band %lu at node %lu: %g\n",
          sars_get_name(sars), (unsigned long)iband, (unsigned long)inode, ka);
        return RES_BAD_ARG;
      }
      if(ks != ks /* NaN? */ || ks < 0) {
        log_err(sars,
          "%s: invalid scattering coefficient for band %lu at node %lu: %g\n",
          sars_get_name(sars), (unsigned long)iband, (unsigned long)inode, ka);
        return RES_BAD_ARG;
      }
    }
  }
  return RES_OK;
}

size_t
sars_get_bands_count(const struct sars* sars)
{
  ASSERT(sars);
  return darray_band_size_get(&sars->bands);
}

size_t
sars_get_nodes_count(const struct sars* sars)
{
  ASSERT(sars);
  return sars->nnodes;
}

res_T
sars_get_band
  (const struct sars* sars,
   const size_t iband,
   struct sars_band* sars_band)
{
  const struct band* band = NULL;
  res_T res = RES_OK;

  if(!sars || !sars_band) {
    res = RES_BAD_ARG;
    goto error;
  }

  if(iband >= sars_get_bands_count(sars)) {
    log_err(sars, "%s: invalid band index %lu.\n",
      FUNC_NAME, (unsigned long)iband);
    res = RES_BAD_ARG;
    goto error;
  }

  band = darray_band_cdata_get(&sars->bands) + iband;
  sars_band->lower = band->low;
  sars_band->upper = band->upp;
  sars_band->id = iband;
  sars_band->k_list = band->k_list;

exit:
  return res;
error:
  goto exit;
}

res_T
sars_find_bands
  (const struct sars* sars,
   const double range[2],
   size_t ibands[2])
{
  const struct band* bands = NULL;
  const struct band* low = NULL;
  const struct band* upp = NULL;
  size_t nbands = 0;
  res_T res = RES_OK;

  if(!sars || !range || !ibands || range[0] > range[1]) {
    res = RES_BAD_ARG;
    goto error;
  }

  bands = darray_band_cdata_get(&sars->bands);
  nbands = darray_band_size_get(&sars->bands);

  low = search_lower_bound(range+0, bands, nbands, sizeof(*bands), cmp_band);
  if(low) {
    ibands[0] = (size_t)(low - bands);
  } else {
    /* The submitted range does not overlap any band */
    ibands[0] = SIZE_MAX;
    ibands[1] = 0;
    goto exit;
  }

  if(range[0] == range[1]) { /* No more to search */
    if(range[0] <  low->low) {
      /* The wavelength is not included in any band */
      ibands[0] = SIZE_MAX;
      ibands[1] = 0;
    } else {
      ASSERT(range[0] < low->upp);
      ibands[1] = ibands[0];
    }
    goto exit;
  }

  upp = search_lower_bound(range+1, bands, nbands, sizeof(*bands), cmp_band);

  /* The submitted range overlaps the remaining bands */
  if(!upp) {
    ibands[1] = nbands - 1;

  /* The upper band includes range[1] */
  } else if(upp->low <= range[1]) {
    ibands[1] = (size_t)(upp - bands);

  /* The upper band is greater than range[1] and therefre must be rejected */
  } else if(upp->low > range[1]) {
    if(upp != bands) {
      ibands[1] = (size_t)(upp - bands - 1);
    } else {
      ibands[0] = SIZE_MAX;
      ibands[1] = 0;
    }
  }

exit:
  return res;
error:
  goto exit;
}

res_T
sars_band_compute_hash
  (const struct sars* sars,
   const size_t iband,
   hash256_T hash)
{
  struct sha256_ctx ctx;
  struct sars_band band;
  res_T res = RES_OK;

  if(!sars || !hash) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = sars_get_band(sars, iband, &band);
  if(res != RES_OK) goto error;

  sha256_ctx_init(&ctx);
  hash_band(&ctx, &band, sars->nnodes);
  sha256_ctx_finalize(&ctx, hash);

exit:
  return res;
error:
  goto exit;
}

res_T
sars_compute_hash(const struct sars* sars, hash256_T hash)
{
  struct sha256_ctx ctx;
  size_t i;
  res_T res = RES_OK;

  if(!sars || !hash) {
    res = RES_BAD_ARG;
    goto error;
  }

  sha256_ctx_init(&ctx);
  sha256_ctx_update(&ctx, (const char*)&sars->pagesize, sizeof(sars->pagesize));
  sha256_ctx_update(&ctx, (const char*)&sars->nnodes, sizeof(sars->nnodes));
  FOR_EACH(i, 0, darray_band_size_get(&sars->bands)) {
    struct sars_band band;
    SARS(get_band(sars, i, &band));
    hash_band(&ctx, &band, sars->nnodes);
  }
  sha256_ctx_finalize(&ctx, hash);

exit:
  return res;
error:
  goto exit;
}

const char*
sars_get_name(const struct sars* sars)
{
  ASSERT(sars);
  return str_cget(&sars->name);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
void
band_release(struct band* band)
{
  ASSERT(band);
  if(!band->k_list) return;

  if(!band->map_len) {
    MEM_RM(band->sars->allocator, band->k_list);
  } else if(band->k_list != MAP_FAILED) {
    munmap(band->k_list, band->map_len);
  }
}

res_T
band_copy(struct band* dst, const struct band* src)
{
  ASSERT(dst && src);

  dst->sars = src->sars;
  dst->low = src->low;
  dst->upp = dst->upp;
  dst->map_len = src->map_len;
  dst->k_list = NULL;

  if(src->map_len) {
    /* The k are mapped: copy the pointer */
    dst->k_list = src->k_list;
  } else if(src->k_list != NULL) {
    /* The k are loaded: duplicate thable contents */
    const size_t memsz = sizeof(*dst->k_list)*src->sars->nnodes*2/*ka & ks*/;
    dst->k_list = MEM_ALLOC(src->sars->allocator, memsz);
    if(!dst->k_list) return RES_MEM_ERR;
    memcpy(dst->k_list, src->k_list, memsz);
  }
  return RES_OK;
}

res_T
band_copy_and_release(struct band* dst, struct band* src)
{
  ASSERT(dst && src);
  dst->sars = src->sars;
  dst->low = src->low;
  dst->upp = dst->upp;
  dst->map_len = src->map_len;
  dst->k_list = src->k_list;
  return RES_OK;
}
