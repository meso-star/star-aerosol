/* Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SARS_C_H
#define SARS_C_H

#include <rsys/dynamic_array.h>
#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

struct band {
  struct sars* sars;
  double low; /* Lower bound in nm (inclusive) */
  double upp; /* Upper bound in nm (exclusive) */
  size_t map_len;
  float* k_list; /* List of float[2] (ka, ks) */
};

static INLINE void
band_init(struct mem_allocator* allocator, struct band* band)
{
  ASSERT(band);
  (void)allocator;
  band->sars = NULL;
  band->low = DBL_MAX;
  band->upp =-DBL_MAX;
  band->map_len = 0;
  band->k_list = NULL;
}

extern LOCAL_SYM void
band_release
  (struct band* band);

extern LOCAL_SYM res_T
band_copy
  (struct band* dst,
   const struct band* src);

extern LOCAL_SYM res_T
band_copy_and_release
  (struct band* dst,
   struct band* src);

/* Generate the dynamic array of bands */
#define DARRAY_NAME band
#define DARRAY_DATA struct band
#define DARRAY_FUNCTOR_INIT band_init
#define DARRAY_FUNCTOR_RELEASE band_release
#define DARRAY_FUNCTOR_COPY band_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE band_copy_and_release
#include <rsys/dynamic_array.h>

struct mem_allocator;

struct sars {
  /* Loaded data */
  uint64_t pagesize;
  uint64_t nnodes;
  struct darray_band bands;

  size_t pagesize_os;
  struct str name;

  struct mem_allocator* allocator;
  struct logger* logger;
  struct logger logger__;
  int verbose;
  ref_T ref;
};

#endif /* SARS_C_H */
